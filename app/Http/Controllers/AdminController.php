<?php

namespace App\Http\Controllers;

use App\Department;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\DepartmentRequest;
use Response;
use Auth;
use Validator;
use File;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function listStaff()
    {
        $users = User::where('id', '!=', Auth::id())->where('role_id', 3)->orderBy('id', 'DESC')->get();
        return Response::json(['data' => $users]);
    }

    public function indexStaff()
    {
        return view('admin.staff');
    }

    public function storeStaff(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = 3;

        $user->save();

        return Response::json(['flash_message' => 'Đã thêm nhân viên!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function showStaff($id)
    {
        $user = User::findOrFail($id);
        return Response::json($user);
    }

    public function updateStaff($id, UserRequest $request)
    {
        if ($request->isMethod('patch')) {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            return Response::json(['flash_message' => 'Đã cập nhật thông tin nhân viên!', 'message_level' => 'success', 'message_icon' => 'check']);
        } else {
            $user = User::findOrFail($id);
            return Response::json($user);
        }
    }

    public function destroyStaff(UserRequest $request)
    {
        if (is_string($request->ids))
            $user_ids = explode(' ', $request->ids);

        foreach ($user_ids as $user_id) {
            if ($user_id != NULL)
                User::findOrFail($user_id)->delete();
        }
        return Response::json(['flash_message' => 'Đã xóa nhân viên!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function listDoctor()
    {
        $users = User::where('id', '!=', Auth::id())->where('role_id', 2)->orderBy('id', 'DESC')->get();
        return Response::json(['data' => $users]);
    }

    public function indexDoctor()
    {
        return view('admin.doctor');
    }

    public function storeDoctor(Request $request)
    {
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = 2;
        $user->khoa = $request->khoa;
        $user->save();
            
        return "Đã tạo bác sỹ thành công";

        // DB::insert('insert into users (name,email,password,role_id,khoa) values (?,?,?,?,?)', 
        //     [ 
        //     $request->name,
        //     $request->email,
        //     bcrypt($request->password),
        //     2,
        //     $request->khoa,
        //     ]);

        return Response::json(['flash_message' => 'Đã thêm bác sĩ!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function showDoctor($id)
    {
        $user = User::findOrFail($id);
        return Response::json($user);
    }

    public function updateDoctor($id, UserRequest $request)
    {
        if ($request->isMethod('patch')) {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            return Response::json(['flash_message' => 'Đã cập nhật thông tin bác sĩ!', 'message_level' => 'success', 'message_icon' => 'check']);
        } else {
            $user = User::findOrFail($id);
            return Response::json($user);
        }
    }

    public function destroyDoctor(UserRequest $request)
    {
        if (is_string($request->ids))
            $user_ids = explode(' ', $request->ids);

        foreach ($user_ids as $user_id) {
            if ($user_id != NULL)
                User::findOrFail($user_id)->delete();
        }
        return Response::json(['flash_message' => 'Đã xóa bác sĩ!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function listDepartment()
    {
        $departments = Department::orderBy('id', 'DESC')->get();
        return Response::json(['data' => $departments]);
    }

    public function indexDepartment()
    {
        return view('admin.department');
    }

    public function storeDepartment(DepartmentRequest $request)
    {
        $department = new Department();
        $department->name = $request->name;
        $department->description = $request->description;
        $department->save();

        return Response::json(['flash_message' => 'Đã thêm khoa!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function showDepartment($id)
    {
        $department = Department::findOrFail($id);
        return Response::json($department);
    }

    public function updateDepartment($id, DepartmentRequest $request)
    {
        if ($request->isMethod('patch')) {
            $department = Department::findOrFail($id);
            $department->name = $request->name;
            $department->description = $request->description;
            $department->save();

            return Response::json(['flash_message' => 'Đã cập nhật thông tin khoa!', 'message_level' => 'success', 'message_icon' => 'check']);
        } else {
            $department = Department::findOrFail($id);
            return Response::json($department);
        }
    }

    public function destroyDepartment(DepartmentRequest $request)
    {
        if (is_string($request->ids))
            $department_ids = explode(' ', $request->ids);

        foreach ($department_ids as $department_id) {
            if ($department_id != NULL)
                Department::findOrFail($department_id)->delete();
        }
        return Response::json(['flash_message' => 'Đã xóa khoa!', 'message_level' => 'success', 'message_icon' => 'check']);
    }
}
