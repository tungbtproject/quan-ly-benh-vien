<?php

namespace App\Http\Controllers;

use App\MedicalApplication;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\MedicalApplicationRequest;
use Response;
use Auth;
use Symfony\Component\VarDumper\Cloner\Data;
use Validator;
use File;
use Session;
use Carbon\Carbon;
use Storage;
use DB;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('staff');
    }
    
    public function index()
    {
        return view('staff.index');
    }

    public function listPatient()
    {
        $users = User::where('role_id', 1)->orderBy('id', 'DESC')->get();
        return Response::json(['data' => $users]);
    }

    public function indexPatient()
    {
        return view('staff.patient');
    }

     public function listAsJson(){

        //return file Json medical list
        //$medical_list_staff = MedicalApplication::where('status',1);
        $medical_list_staff = DB::table('users')
            ->join('medical_applications', 'medical_applications.user_id', '=', 'users.id')
            ->select('medical_applications.id','medical_applications.date','users.name','medical_applications.status')
            ->where('status',1)
            ->get();
        return $medical_list_staff;  


    }
//đây là cái cũ, cái này có giá trị trả về hàm bên trên thì ko
     public function listAsJson2(){

        //return file Json medical list
        $medical_list_staff = MedicalApplication::where('status',1)->get();
     
        return $medical_list_staff;    
    }

    public function storePatient(UserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = 1;
        $user->save();

        return Response::json(['flash_message' => 'Đã thêm bệnh nhân!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function showPatient($id)
    {
        $user = User::findOrFail($id);
        return Response::json($user);
    }

    public function updatePatient($id, UserRequest $request)
    {
        if ($request->isMethod('patch')) {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            return Response::json(['flash_message' => 'Đã cập nhật thông tin bệnh nhân!', 'message_level' => 'success', 'message_icon' => 'check']);
        } else {
            $user = User::findOrFail($id);
            return Response::json($user);
        }
    }

    public function destroyPatient(UserRequest $request)
    {
        if (is_string($request->ids))
            $user_ids = explode(' ', $request->ids);

        foreach ($user_ids as $user_id) {
            if ($user_id != NULL)
                User::findOrFail($user_id)->delete();
        }
        return Response::json(['flash_message' => 'Đã xóa bệnh nhân!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function storeMedicalApplication(MedicalApplicationRequest $request)
    {
        $medical_application = new MedicalApplication();

        $medical_application = new MedicalApplication();
        $medical_application->user_id = $request->userId;
        $medical_application->status = 1;
        $url = Carbon::now()->toDateString() .'-'. Auth::user()->id . '-' . substr(sha1(rand()), 0, 15) . '.xml';
        $medical_application->url = $url;
        $medical_application->date = date("Y-m-d H:i:s");
        $medical_application->save();
        Storage::copy('donkham.xml', $url);

        return Response::json(['flash_message' => 'Đã đăng ký khám cho bệnh nhân này!', 'message_level' => 'success', 'message_icon' => 'check']);
    }

    public function listMedicalApplication()
    {
        $users = User::where('role_id', 1)->orderBy('id', 'DESC')->get();
        return Response::json(['data' => $users]);
    }

    public function indexMedicalApplication()
    {
        return view('staff.MedicalApplication');
    }

    public function showMedicalApplication($id)
    {
        $user = User::findOrFail($id);
        return Response::json($user);
    }

    public function updateMedicalApplication($id, UserRequest $request)
    {
        if ($request->isMethod('patch')) {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            if (isset($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->save();

            return Response::json(['flash_message' => 'Đã cập nhật thông tin bệnh nhân!', 'message_level' => 'success', 'message_icon' => 'check']);
        } else {
            $user = User::findOrFail($id);
            return Response::json($user);
        }
    }

    public function destroyMedicalApplication(UserRequest $request)
    {
        if (is_string($request->ids))
            $user_ids = explode(' ', $request->ids);

        foreach ($user_ids as $user_id) {
            if ($user_id != NULL)
                User::findOrFail($user_id)->delete();
        }
        return Response::json(['flash_message' => 'Đã huỷ đơn khám!', 'message_level' => 'success', 'message_icon' => 'check']);
    }
}
