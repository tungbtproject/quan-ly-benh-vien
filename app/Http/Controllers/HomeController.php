<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return redirect()->route('patient-index');
        if(Auth::user()->role_id == 1){
            return redirect()->route('patient-index');
        }
        if(Auth::user()->role_id == 2){
            return redirect()->route('doctor-index');
        }        
        if(Auth::user()->role_id == 3){
            return redirect()->route('StaffController.index');
        }
        if(Auth::user()->role_id == 4){
            return redirect()->route('AdminController.index');
        }
        return view('home');
    }
}
