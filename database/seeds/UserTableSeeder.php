<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;
use App\MedicalApplication;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('medical_applications')->delete();

        User::create([
                'name' => "Quản trị viên",
                'email' => "admin@gmail.com",
                'password' => bcrypt("123456"),
                'role_id' => 4,
                'avatar' => 'default.jpg'
            ]);
        echo PHP_EOL."Da tao admin";
        User::create([
                'name' => "Nhân viên",
                'email' => "nhanvien@gmail.com",
                'password' => bcrypt("123456"),
                'role_id' => 3,
                'avatar' => 'default.jpg'
            ]);
        echo PHP_EOL."Da tao nhan vien";
        User::create([
                'name' => "Bác sỹ",
                'email' => "bacsy@gmail.com",
                'password' => bcrypt("123456"),
                'role_id' => 2,
                'avatar' => 'default.jpg'
            ]);
        echo PHP_EOL."Da tao bac sy";
        $faker = Faker\Factory::create();
        foreach( range(1,100) as $index ){
            User::create([
            'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'role_id' => 1,
                'avatar' => 'default.jpg'
            ]);
            echo PHP_EOL."Dang tao benh nhan thu ".$index;
            User::create([
            'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'role_id' => 2,
                'avatar' => 'default.jpg'
            ]);
            echo PHP_EOL."Dang tao bac sy thu ".$index;
            User::create([
            'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'role_id' => 3,
                'avatar' => 'default.jpg'
            ]);
            echo PHP_EOL."Dang tao nhan vien thu ".$index;
        } 
        $users = User::all();
        foreach($users as $user){
            foreach( range(1,10) as $index ){
                $medical_application = new MedicalApplication();
                $medical_application->user_id = $user->id;
                $medical_application->status = 1;
                $url = Carbon::now()->toDateString() .'-'. $user->id . '-' . substr(sha1(rand()), 0, 15) . '.xml';
                $medical_application->url = $url;
                $medical_application->date = date("Y-m-d H:i:s");
                $medical_application->save();
                echo PHP_EOL."Dang tao don kham cho benh nhan thu ". $user->id;
                
            }
        }
        
    }
}
