@extends('doctor.layout')
@section('title')
Tìm kiếm thông tin bệnh nhân
@stop
@section('content')
	 <script src="{{ URL::asset('themes/assets/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('themes/assets/bootstrap-table/src/bootstrap-table.js') }}"></script>
	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<table data-toggle="table" data-url="{{ route('search-as-json') }}"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						       
						        
						        <th data-field="name"  data-sortable="true">Họ tên</th>
                                <th data-field="birthday" data-formatter="dateFormatter" data-sortable="true">Ngày sinh</th>
                                <th data-field="personal_history"  data-sortable="true">Tiền sử bệnh tật</th>
                                <th data-field="family_history"  data-sortable="true">Tiền sử bệnh tật gia đình</th>
                                <th data-field="id" 
                                	data-sortable="true" 
                                	data-formatter="operateFormatter"
                        			data-events="operateEvents">Xem chi tiết</th>

						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Chi tiết</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tableHistory" data-toggle="table"
		                       data-url="">
		                    <thead>
		                    <tr>
		                        
		                        <th data-field="date"
		                        	data-formatter="dateFormatter">Ngày khám</th>
		                        <th data-field="id"
		                        	data-formatter="xemFormatter"
		                        	data-events="xemEvents">Xem chi tiết</th>
		                    </tr>
		                    </thead>
		                </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modalXemTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Chi tiết</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tableDetail" 
                        	   data-toggle="table"
                       		   data-url="">
                    <thead>
                    <tr>
                        <th data-field="thong_tin">
                        Xét nghiệm
                        </th>
                        <th data-field="chi_so">
                        Kết quả
                        </th>
                    </tr>
                    </thead>
                </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        
<script>
	var $tableHistory = $('#tableHistory');
	var $tableDetail = $('#tableDetail');

    window.operateEvents = {
        'click .like': function (e, value, row) {
        	
            $tableHistory.bootstrapTable('refresh', {
    			url:  './view-patient-history-by-doctor.json/' + value
			});
			
        },
        'click .remove': function (e, value, row) {
           document.getElementById('medical_id').setAttribute("value",value)
        }
    };

    function operateFormatter(value, row, index) {
        return [
            // '<div class="pull-left">',
            // '<a href="/details/' + value + '" target="_blank">' + 'Xem' + '</a>',
            // '</div>',
            '<div class="">',
            '<a class="like" href="javascript:void(0)" title="Like">',
            '<button class="btn btn-primary" data-toggle="modal" data-target="#modalTable">Xem</button>',
            '</a>  ',
            
            '</div>'
        ].join('');
    }
    window.xemEvents = {
        'click .xem': function (e, value, row) {
        	
            $tableDetail.bootstrapTable('refresh', {
    			url:  './view-medical-application-detail-by-doctor.json/' + value
			});
			
        },
        
    };
    function xemFormatter(value, row, index) {
        return [
            // '<div class="pull-left">',
            // '<a href="/details/' + value + '" target="_blank">' + 'Xem' + '</a>',
            // '</div>',
            '<div class="">',
            '<a class="xem" href="javascript:void(0)" title="Like">',
            '<button class="btn btn-primary" data-toggle="modal" data-target="#modalXemTable">Xem</button>',
            '</a>  ',
            
            '</div>'
        ].join('');
    }
    function dateFormatter(value, row, index){
    	return[
    		value.substring(0,10)
    	]
    }
</script>	
 
@stop