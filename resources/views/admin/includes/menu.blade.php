<li {{ (Request::is('admin/index') ? 'class=active' : '') }}>
    <a href="index">
        <svg class="glyph stroked dashboard-dial">
            <use xlink:href="#stroked-dashboard-dial"></use>
        </svg>
        Chào mừng
    </a>
</li>

<li {{ (Request::is('admin/doctor') ? 'class=active' : '') }}>
    <a href="doctor"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Quản lý bác sỹ</a>
</li>
<li {{ (Request::is('admin/staff') ? 'class=active' : '') }}>
    <a href="staff"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Quản lý nhân viên</a>
</li>
