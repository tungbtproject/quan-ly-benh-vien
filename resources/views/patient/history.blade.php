@extends('patient.layout')
@section('title')
   Sổ khám bệnh
@stop
@section('content')

    <script src="{{ URL::asset('themes/assets/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('themes/assets/bootstrap-table/src/bootstrap-table.js') }}"></script>




        <div class="row">
            <div class="col-md-12">
                <table id="table" data-toggle="table"
                       data-url="{{ route('history-json') }}">
                    <thead>
                    <tr>
                        
                        <th data-field="date"
                        	data-formatter="dateFormatter">Ngày khám</th>
                        <th data-field="id"
                        	data-formatter="operateFormatter"
                        	data-events="operateEvents">Xem chi tiết</th>
                    </tr>
                    </thead>
                </table>
            </div>



        <div class="modal fade" id="modalTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Chi tiết</h4>
                    </div>
                    <div class="modal-body">
                        <table id="table-detail" 
                        	   data-toggle="table"
                       		   data-url="">
                    <thead>
                    <tr>
                        <th data-field="thong_tin">
                        Xét nghiệm
                        </th>
                        <th data-field="chi_so">
                        Kết quả
                        </th>
                    </tr>
                    </thead>
                </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="removeMedicalApp" tabindex="-1" role="dialog" aria-labelledby="removeMedicalAppLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Xóa đơn khám?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Bạn chắc chắn muốn xóa đơn khám này chứ?</p>
                    </div>
                    <div class="modal-footer">

                            <a
                               onclick="event.preventDefault();
                                                 document.getElementById('remove-medical-form').submit();">
                                
                                <button class="btn btn-danger" id="btn-delete-user">Xóa</button>
                            </a>

                            <form id="remove-medical-form" action="{{ url('/remove-medical-application') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                <input type="hidden" id="medical_id" name="medical_id" value="">
                            </form>
                    </div>
                </div>
            </div>
        </div>
        


<script>
	var $table = $('#table-detail');

    window.operateEvents = {
        'click .like': function (e, value, row) {
        	//alert('Trying to refresh new url ' + value);
            $table.bootstrapTable('refresh', {
    			url:  '../../patient/detail.json/' + value
			});
        },
        'click .remove': function (e, value, row) {
           document.getElementById('medical_id').setAttribute("value",value)
        }
    };

    function operateFormatter(value, row, index) {
        return [
            // '<div class="pull-left">',
            // '<a href="/details/' + value + '" target="_blank">' + 'Xem' + '</a>',
            // '</div>',
            '<div class="">',
            '<a class="like" href="javascript:void(0)" title="Like">',
            '<button class="btn btn-primary" data-toggle="modal" data-target="#modalTable">Xem</button>',
            '</a>  ',
            '<a class="remove" href="javascript:void(0)" title="Xóa">',
            '<button class="btn btn-danger" data-toggle="modal" data-target="#removeMedicalApp">Xóa</button>',
            '</a>',
            '</div>'
        ].join('');
    }
    function dateFormatter(value, row, index){
    	return[
    		value.substring(0,10)
    	]
    }
</script>		

@stop